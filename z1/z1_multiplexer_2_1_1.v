module z1_multiplexer_2_1_1 (
	input sel, 
	input d0, 
	input d1, 
	output reg y
);

	always @ (*) begin
		if (!sel) y = d0;
		else y = d1;
	end
endmodule