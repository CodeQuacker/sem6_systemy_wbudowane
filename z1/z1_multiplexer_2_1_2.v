module z1_multiplexer_2_1_2 (
	input  sel, 
	input  [0:1] d0, 
	input  [0:1] d1, 
	output reg [0:1] y
);

always @ (*)
	if (!sel) y = d0;
	else y = d1;
endmodule