// ---------------------- MUX 2:1 1bit ----------------------
//module ps2 (
//	input  [1:0] SW, 
//	input  [0:0] KEY,
//	output [0:0] LEDR
//);
//z1_multiplexer_2_1_1 mux2to1 (.sel(KEY[0]), .d0(SW[0]), .d1(SW[1]), .y(LEDR[0]));
//endmodule

// ---------------------- MUX 4:1 1bit ----------------------
//module ps2 (
//	input  [3:0] SW, 
//	input  [1:0] KEY,
//	output [0:0] LEDR
//);
//wire mux1_out, mux2_out, mux_out;
//z1_multiplexer_2_1_1 mux2to1_1 (.sel(KEY[0]), .d0(SW[0]), .d1(SW[2]), .y(mux1_out));
//z1_multiplexer_2_1_1 mux2to1_2 (.sel(KEY[0]), .d0(SW[1]), .d1(SW[3]), .y(mux2_out));
//z1_multiplexer_2_1_1 mux4to1 (.sel(KEY[1]), .d0(mux1_out), .d1(mux2_out), .y(LEDR[0]));
//endmodule

// ---------------------- MUX 4:1 2bit ----------------------
//module ps2 (
//	input  [7:0] SW,
//	input [1:0] KEY,
//	output [0:1] LEDR
// );
//
//z1_multiplexer_4_1_2 mux4to1 (.sel({KEY[0], KEY[1]}), .d0({SW[1], SW[0]}), .d2({SW[3], SW[2]}), .d1({SW[5], SW[4]}), .d3({SW[7], SW[6]}), .y0(LEDR[1]), .y1(LEDR[0]));
//
//assign LEDR0 = mux_out1;
//assign LEDR1 = mux_out0;
//endmodule

// ------------------ HEX to 2 7-seg display ------------------
module ps2 (
	input  [7:0] SW,
	output [0:6] HEX0,
	output [0:6] HEX1
  );
  
decoder_hex_16_left left (.x(SW[7:4]), .segments(HEX1[0:6]));
decoder_hex_16_right right (.x(SW[3:0]), .segments(HEX0[0:6]));
endmodule


// ------------------ binary_BCD_4_bits ------------------
//module ps2 (
//	input  [7:0] SW,
//	output [0:6] HEX0,
//	output [0:6] HEX1
// );
// 
//decoder_bcd left (.x(SW[7:4]), .segments(HEX1[0:6]));
//decoder_bcd right (.x(SW[3:0]), .segments(HEX0[0:6]));
//endmodule
