module decoder_hex_16_right(
	input [3:0] x, 
	output reg [0:6] segments
);

 always @(*)
	  case (x)
			4'h0: segments = 7'b0000001;
			4'h1: segments = 7'b1001111;
			4'h2: segments = 7'b0010010;
			4'h3: segments = 7'b0000110;
			4'h4: segments = 7'b1001100;
			4'h5: segments = 7'b0100100;
			4'h6: segments = 7'b0100000;
			4'h7: segments = 7'b0001111;
			4'h8: segments = 7'b0000000;
			4'h9: segments = 7'b0000100;
			4'ha: segments = 7'b0001000;
			4'hb: segments = 7'b1100000;
			4'hc: segments = 7'b0110001;
			4'hd: segments = 7'b1000010;
			4'he: segments = 7'b0110000;
			4'hf: segments = 7'b0111000;
	  endcase
endmodule