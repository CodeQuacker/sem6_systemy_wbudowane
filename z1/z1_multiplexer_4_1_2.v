module z1_multiplexer_4_1_2 (
  input [0:1] sel,
  input [0:1] d0,
  input [0:1] d1,
  input [0:1] d2,
  input [0:1] d3,
  output reg [0:1] y
);

always @(*)
	case (sel)
		 2'b00: y = d0;
		 2'b01: y = d1;
		 2'b10: y = d2;
		 2'b11: y = d3;
	endcase
endmodule