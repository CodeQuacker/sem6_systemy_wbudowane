module counter8bit (
    input clk,
    input areset,
    input enable,
    output reg [7:0] Q
);
    always @(posedge clk or posedge areset) begin
        if (areset)
            Q <= 8'b00000000;
        else if (enable)
            Q <= Q + 1;
    end
endmodule