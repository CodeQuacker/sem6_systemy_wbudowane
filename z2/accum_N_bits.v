module accum_N_bits #(
    parameter N = 8
) (
    input [N-1:0] A,
    input clk,
    input aclr,
    output reg [N-1:0] S,
    output reg carry,
    output reg overflow
);
    wire [N-1:0] B, sum;
    wire buffer_carry;

    assign B = aclr ? 0 : S;
    assign {buffer_carry, sum} = A + B;
    
    // output S and carry
    always @(posedge clk or posedge aclr)
        if (aclr) begin
            carry <= 0;
            S <= 0;
        end else begin
            carry <= buffer_carry;
            S <= sum;
        end
    
    // output overflow
    always @(posedge clk or posedge aclr)
        if (aclr) overflow <= 0;
        else overflow <= (A[N-1] == B[N-1]) && (A[N-1] != sum[N-1]);
endmodule
