module fsm_one_hot (
    input w,
    input clk,
    input aclr,
    output reg z,
    output [8:0] y
);
    reg [8:0] state;

    localparam [8:0] A = 9'b000000001;
    localparam [8:0] B = 9'b000000010;
    localparam [8:0] C = 9'b000000100;
    localparam [8:0] D = 9'b000001000;
    localparam [8:0] E = 9'b000010000;
    localparam [8:0] F = 9'b000100000;
    localparam [8:0] G = 9'b001000000;
    localparam [8:0] H = 9'b010000000;
    localparam [8:0] I = 9'b100000000;

    always @(posedge clk or posedge aclr)
        if (aclr) state <= A;
        else
            case (state)
                A: state <= !w ? B : F;
                B: state <= !w ? C : F;
                C: state <= !w ? D : F;
                D: state <= !w ? E : F;
                E: state <= !w ? E : F;
                F: state <= w ? G : B;
                G: state <= w ? H : B;
                H: state <= w ? I : B;
                I: state <= w ? I : B;
                default: state <= A;
            endcase

    always @(*)
        if (state == E || state == I) z = 1'b1;
        else z = 1'b0;
    assign y = state;
endmodule
