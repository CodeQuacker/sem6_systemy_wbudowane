
// ==================== zad1 ========================
// module ps3(
//     input  [0:0] KEY,
//     input  [1:0] SW,
//     output [6:0] HEX0,
//     output [6:0] HEX1
// );

// wire [7:0] count;

// counter8bit counter(
//     .clock(KEY),
//     .areset(SW[0]),
//     .enable(SW[1]),
//     .Q(count)
// );

// decoder_hex_16 decoder1(.x(count[3:0]),  .segments(HEX0[6:0]))// decoder_hex_16 decoder2(.x(count[7:4]),  .segments(HEX1[6:0]))
// endmodule

// ==================== zad2 ========================
// module ps3 (
//     input CLOCK_50,
//     output [6:0] HEX0
// );
//     wire [3:0] count;
//     wire enable_modulo;

//     counter_mod #(
//         // zmniejszone dla symulacji, normalnie 5_000_000
//         .M(5)
//     ) delay_counter (
//         .clock(CLOCK_50),
//         .reset(1'h0),
//         .enable(1'h1),
//         .count_out(),
//         .tick(enable_modulo)
//     );

//     counter_mod #(
//         .M(10)
//     ) modulo_10_counter (
//         .clock(CLOCK_50),
//         .reset(1'h0),
//         .enable(enable_modulo),
//         .count_out(count),
//         .tick()
//     );
//     decoder_hex_16 dekoder (
//         .x(count),
//          .segments(HEX0//     );
// endmodule

// ==================== zad3 ========================
// module ps3 (
//     input  [7:0] SW,
//     input  [1:0] KEY,
//     output [9:0] LEDR,
//     output [6:0] HEX0,
//     output [6:0] HEX1,
//     output [6:0] HEX2,
//     output [6:0] HEX3
// );

//     wire [7:0] A;
//     wire [7:0] S;

//     assign A = SW[7:0];
//     assign LEDR[7:0] = S[7:0];

//     accum_N_bits accumulator (
//         .A(A),
//         .clk(KEY[1]),
//         .aclr(KEY[0]),
//         .S(S),
//         .carry(LEDR[8]),
//         .overflow(LEDR[9])
//     );

//     decoder_hex_16 hex3 (.x(A[7:4]), .segments(HEX3));
//     decoder_hex_16 hex2 (.x(A[3:0]), .segments(HEX2));
//     decoder_hex_16 hex1 (.x(S[7:4]), .segments(HEX1));
//     decoder_hex_16 hex0 (.x(S[3:0]), .segments(HEX0));

// endmodule

// ==================== zad4 ========================
module ps3 (
    input  [1:0] SW,
    input  [0:0] KEY,
    output [9:0] LEDR
);

    fsm_one_hot fsm (
        .w(SW[0]),
        .clk(KEY[0]),
        .aclr(SW[1]),
        .z(LEDR[9]),
        .y(LEDR[8:0])
    );

endmodule
