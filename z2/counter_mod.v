module counter_mod #(
    parameter M = 4
) (
    input clock,
    input reset,
    input enable,
    output [N-1:0] count_out,
    output tick
);
    reg  [N-1:0] current;
    wire [N-1:0] next;
    localparam N = clogb2(M);

    function integer clogb2;
        input [31:0] v;
        for (clogb2 = 0; v > 0; clogb2 = clogb2 + 1) v = v >> 1;
    endfunction

    always @(posedge clock or posedge reset)
        if (reset) current <= 0;
        else if (enable) current <= next;
    assign next = (current == (M - 1)) ? 0 : current + 1'b1;

    assign count_out = current;
    assign tick = current == 0;
endmodule